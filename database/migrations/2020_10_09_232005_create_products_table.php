<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('account_id');
            $table->foreignId('category_id');
            $table->foreignId('unit_id');
            $table->foreignId('user_id');

            $table->boolean('is_product')->default(true); //si es servicio o producto
            $table->string('product_key');  //codigo de producto
            $table->text('note')->nullable();   // descripcion del producto o nombre
            $table->decimal('cost', 13, 2);
            $table->decimal('quantity', 13, 2)->nullable();  //para manejo de inventario

            $table->unsignedInteger('public_id')->index();
            $table->unique( array('account_id','public_id'));
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('unit_id')->references('id')->on('units');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
