<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;
use App\Models\Unit;
class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'account_id' => 1,
            'user_id' => 1,
            'category_id' => $this->faker->numberBetween($min=1,$max=Category::where('account_id',1)->max('id')),
            'unit_id' => $this->faker->numberBetween($min=1,$max=Unit::where('account_id',1)->max('id')),
            'product_key' => $this->faker->name,
            'note' => $this->faker->name,
            'cost' => $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'quantity' => $this->faker->numberBetween($min=100,$max=1000),

        ];
    }
}
