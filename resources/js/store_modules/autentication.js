const aplication = { tokken: "", user: {}, account: {}, branches: [], branch: null }
try {
  aplication.token = localStorage.getItem("token")
  aplication.user = JSON.parse(localStorage.getItem("user"))
  aplication.account = JSON.parse(localStorage.getItem("account"))
  aplication.branches = JSON.parse(localStorage.getItem("branches"))
  aplication.branch = JSON.parse(localStorage.getItem("branch")) || aplication.branches[0]
} catch (error) {
  aplication.token = ""
  aplication.user = {}
  aplication.account = {}
  aplication.branches = []
  aplication.branch = null
}

export const autentication = {
  namespaced: true,
  state: {
    status: "",
    token: aplication.token,
    user: aplication.user,
    account: aplication.account,
    branches: aplication.branches,
    branch: aplication.branch
  },
  mutations: {
    auth_request (state) {
      state.status = "loading"
    },
    auth_success (state, { token, user, account, branches, branch }) {
      state.status = "success"
      state.token = token
      state.user = user
      state.account = account
      state.branches = branches
      state.branch = branch
    },
    updatedBranch (state, branch) {
      state.branch = branch
      console.log(state.branch)
    },

    auth_error (state) {
      state.status = "error"
    },
    logout (state) {
      state.status = ""
      state.token = ""
    }
  },
  actions: {
    login ({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request")
        // eslint-disable-next-line no-undef
        axios({ url: "api/login", data: user, method: "POST" })
          .then(response => {
            const token = response.data.token
            const user = response.data.user
            const account = response.data.account
            const branches = response.data.branches
            const branch = response.data.branch ? response.data.branch : response.data.branches[0]

            localStorage.setItem("token", token)
            localStorage.setItem("user", JSON.stringify(user))
            localStorage.setItem("account", JSON.stringify(account))
            localStorage.setItem("branches", JSON.stringify(branches))
            localStorage.setItem("branch", JSON.stringify(branch))
            // eslint-disable-next-line no-undef
            axios.defaults.headers.common.Authorization = `Bearer ${token}`
            commit("auth_success", { token, user, account, branches, branch })
            resolve(response)
          })
          .catch(err => {
            commit("auth_error")
            localStorage.removeItem("token")
            localStorage.removeItem("user")
            localStorage.removeItem("account")
            localStorage.removeItem("branches")
            localStorage.removeItem("branch")
            reject(err)
          })
      })
    },
    branch_update ({ commit }, branch) {
      localStorage.setItem("branch", JSON.stringify(branch))
      // eslint-disable-next-line no-undef
      axios.post(`/api/auth/change_branch`, { branch_id: branch.id })
        .then(response => {
          commit("updatedBranch", branch)
          // eslint-disable-next-line no-undef
          iziToast.info({
            title: "",
            message: response.data.message,
            position: "topRight" // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
          })
        })
        .catch(err => {
        // eslint-disable-next-line no-undef
          iziToast.error({
            title: "No se pudo Cambiar de Sucursal",
            message: err,
            position: "topRight" // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
          })
        })
    },
    logout ({ commit }) {
      return new Promise((resolve, reject) => {
        commit("logout")
        localStorage.removeItem("token")
        localStorage.removeItem("user")
        localStorage.removeItem("account")
        localStorage.removeItem("branches")
        localStorage.removeItem("branch")
        // eslint-disable-next-line no-undef
        delete axios.defaults.headers.common.Authorization
        resolve()
      })
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    userLoged: state => state.user
  }
}
