import Home from "./components/Home"
import Login from "./components/auth/Login"
import Client from "./components/client/Index"
import ClientEdit from "./components/client/Edit"
import Product from "./components/product/Index"
import ProductEdit from "./components/product/Edit"
import Category from "./components/category/Index"
import Unit from "./components/unit/Index"
import Invoice from "./components/invoice/Index"
import Payment from "./components/payment/Index"
import Note from "./components/note/Index"
import NoteEdit from "./components/note/Edit"
export const routes = [
  {
    path: "/",
    name: "Dashboard",
    components: {
      default: Home
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ name: "Inicio" }],
      mainRoot: "/"
    }
  },
  {
    path: "/login",
    name: "Inicar Sesion",
    component: Login
  },
  {
    path: "/client",
    name: "Clientes",
    components: {
      default: Client
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ name: "Clientes" }],
      mainRoot: "/client"
    }
  },
  {
    path: "/client/:id/edit",
    name: "Editar Cliente",
    components: {
      default: ClientEdit
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ path: "/client", name: "Clientes" }, { name: "Editar" }],
      mainRoot: "/client"
    }
  },
  {
    path: "/product",
    name: "Productos",
    components: {
      default: Product
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ name: "Productos" }],
      mainRoot: "/product"
    }
  },
  {
    path: "/product/:id/edit",
    name: "Editar Producto",
    components: {
      default: ProductEdit
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ path: "/product", name: "Productos" }, { name: "Editar" }],
      mainRoot: "/product"
    }
  },

  {
    path: "/product/category",
    name: "Categorias",
    components: {
      default: Category
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ path: "/product", name: "Productos" }, { name: "Categorias" }],
      mainRoot: "/product"
    }
  },

  {
    path: "/product/unit",
    name: "Unidades",
    components: {
      default: Unit
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ path: "/product", name: "Productos" }, { name: "Unidades" }],
      mainRoot: "/product"
    }
  },

  {
    path: "/invoice",
    name: "Facturas",
    components: {
      default: Invoice
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: []
    }
  },
  {
    path: "/note",
    name: "Notas",
    components: {
      default: Note
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ name: "Notas" }],
      mainRoot: "/note"
    }
  },
  {
    path: "/note/:id/edit",
    name: "Editar Nota",
    components: {
      default: NoteEdit
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ path: "/note", name: "Notas" }, { name: "Editar" }],
      mainRoot: "/note"
    }
  },
  {
    path: "/payment",
    name: "Pagos",
    components: {
      default: Payment
    },
    meta: {
      requiresAuth: true,
      breadcrumbs: [{ name: "Pagos" }],
      mainRoot: "/payment"
    }
  }
]
