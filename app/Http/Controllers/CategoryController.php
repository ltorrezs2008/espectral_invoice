<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Auth;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sort_name= request('sort_name')??'first_name';
        $pagination_rows =request('pagination_rows')??10;
        $conditions=[];

        $categories = Category::where($conditions)
        ->orderBy('created_at','desc')
         ->paginate($pagination_rows);
        // return response()->json([
        //     'categories' => $categories->toArray(),
        //     'total'=>$total,
        // ]);

        return response()->json(compact('categories'));
    }

    /** list for multiselect */
    public function list()
    {
        $categories = Category::select('id','name')->get();
        return response()->json(compact('categories'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $message = "Categoria Actualizada";
        $type_message = "info";

        if($request->id==0)
        {
            $message = "Categoria Creada";
            $type_message = "success";
            $category = new Category;
        }else{

            $category = Category::find($request->id);
        }

        $category->account_id = Auth::user()->account_id;
        $category->user_id = Auth::user()->id;
        $category->name = $request->name;
        $category->description = $request->description;

        $category->save();

        return response()->json(compact('message','type_message'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $category = Category::find($id);
        $category->delete();
        $message = "Categoria Eliminada";
        $type_message = "info";
        return response()->json(compact('message','type_message'));
    }
}
