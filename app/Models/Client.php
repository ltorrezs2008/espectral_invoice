<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Contact;

class Client extends Model
{
    use HasFactory;


    public function contacts()
    {
        return $this->hasMany('App\Models\Contact','client_id','id');
    }
    // public function contact()
    // {
    //     return Contact::where('client_id',$this->id)->first()??new Client;
    // }
}
