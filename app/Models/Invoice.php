<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Util;
class Invoice extends Model
{
    use HasFactory;

    protected $appends = array('amount');

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function invoice_items()
    {
        return $this->hasMany('App\Models\InvoiceItem')->with('product');
    }

    public function invoice_status()
    {
        return $this->belongsTo('App\Models\InvoiceStatus');
    }

    public function getAmountAttribute()
    {
        $amount =0;
        foreach ($this->invoice_items as $item) {
            $amount += (float) $item->quantity * $item->cost;
        }
        return  Util::twoDecimals($amount);
    }
}
